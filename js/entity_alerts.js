(function ($) {
  Drupal.behaviors.entityAlerts = {
    attach: function (context, settings) {
      $("input.delete-alert").click(function() {
        $(".form-item-entity-alerts-date-time input").val('');
        $(".form-item-entity-alerts-user input").val('');
        $(".form-item-entity-alerts-remark textarea").html('');
      });
    }
  };
})(jQuery);
