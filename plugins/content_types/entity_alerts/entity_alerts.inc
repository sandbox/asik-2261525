<?php
/**
 * @file
 * Ctools CT plugin for render Hello button on main dashboard.
 */

$plugin = array(
  'single' => TRUE,
  'title' => t('Entity alerts.'),
  'description' => t('Entity alerts.'),
  'category' => t('Entity Alerts'),
);

/**
 * Render the custom content type.
 */
function entity_alerts_entity_alerts_content_type_render($subtype, $conf, $panel_args, $context) {
  $block = new stdClass();
  $content = '';
  $alerts = entity_alerts_get_all_alerts();
  if ($alerts) {
    $header = array(
      t('Date and time'),
      t('Alert remark text'),
      t('Assigned to'),
      t('Operations'),
      ''
    );
    foreach ($alerts as $alert) {
      $user = user_load($alert->uid);
      if ($user) {
        $user = $user->name;
      }
      else {
        $user = '-';
      }
      $rows[] = array(
        format_date($alert->date, 'custom', 'd-m-Y H:i'),
        $alert->remark,
        $user,
        // Need update logic to other entity support.
        array('data' => l('Edit node', 'node/' . $alert->entity_id . '/edit', array('query' => drupal_get_destination()))),
        array('data' => l('Delete alert', 'entity-alert/remove/' . $alert->aid, array('query' => drupal_get_destination()))),
      );
    }
    $content = theme('table', array('header' => $header, 'rows' => $rows));
  }
  $block->content = $content;
  return $block;
}
